import React from 'react';
import {IndexRoute, Route} from 'react-router';
import {
  App,
  NotFound,
  Home
} from 'containers';

export default () => {
  const routes = (
    <Route path="/" component={App}>
      { /* Home (main) route */ }
      <IndexRoute component={Home}/>

      { /* Catch all route */ }
      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
  return routes;
};
