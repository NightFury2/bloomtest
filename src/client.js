import React from 'react';
import { render } from 'react-dom';
// import GoogleAnalytics from 'react-ga';

import { browserHistory } from 'react-router';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import { Root } from 'containers';
import rootSaga from './sagas';
import getRoutes from './routes';
import configureStore from './store/configureStore';
import { syncHistoryWithStore} from 'react-router-redux';
import useScroll from 'scroll-behavior/lib/useStandardScroll';

const _browserHistory = useScroll(() => browserHistory)();
const dest = document.getElementById('content');
const store = configureStore(window.__data); // eslint-disable-line
const history = syncHistoryWithStore(_browserHistory, store);

// GoogleAnalytics.initialize(config.app.googleAnalytics.appId);

store.runSaga(rootSaga);

class Main extends React.Component {
  // Remove the server-side injected CSS.
  componentDidMount() {
    const jssStyles = document.getElementById('jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    return <Root store={store} history={history} routes={getRoutes(store)} {...this.props}/>;
  }
}

// Create a theme instance.
const theme = createMuiTheme();

render(
  <MuiThemeProvider theme={theme}>
    <Main />
  </MuiThemeProvider>,
  dest
);

if (process.env.NODE_ENV !== 'production') {
  window.React = React; // enable debugger
}
