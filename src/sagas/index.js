import { fork } from 'redux-saga/effects';
import 'babel-polyfill';

import * as serverInfo from './serverInfo';

export default function* rootSaga() {
  yield fork(serverInfo.serverInfoFlow);
}
