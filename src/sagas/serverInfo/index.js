import { take, call, put } from 'redux-saga/effects';
import axios from 'axios';
import {formatUrl} from '../../helpers/apiHelpers';

import {
  LOAD_SERVER_INFO
} from '../../actions/actionTypes';
import * as actions from '../../actions/serverInfo';

function getServerData() {
  const url = formatUrl('loadInfo');
  return axios({
    method: 'get',
    url: url
  })
    .then(response => response.data);
}

export function* getServerInfo() {
  try {
    const infoData = yield call(getServerData);
    yield put(actions.loadInfoSuccess(infoData.message, infoData.time));
  } catch (err) {
    yield put(actions.loadInfoFail(err));
  }
}

export function* serverInfoFlow() {
  for (;;) {
    yield take(LOAD_SERVER_INFO);
    yield call(getServerInfo);
  }
}
