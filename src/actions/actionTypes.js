import config from '../config';

export const LOAD_SERVER_INFO = 'LOAD_SERVER_INFO';
export const LOAD_SERVER_INFO_SUCCESS = 'LOAD_SERVER_INFO_SUCCESS';
export const LOAD_SERVER_INFO_FAIL = 'LOAD_SERVER_INFO_FAIL';
export const API_HOST = 'http://' + config.apiHost + ':' + config.apiPort;
