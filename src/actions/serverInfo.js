import {
  LOAD_SERVER_INFO,
  LOAD_SERVER_INFO_SUCCESS,
  LOAD_SERVER_INFO_FAIL
} from './actionTypes';

export function loadInfo() {
  return {
    type: LOAD_SERVER_INFO
  };
}

export function loadInfoSuccess(message, time) {
  return {
    type: LOAD_SERVER_INFO_SUCCESS,
    message,
    time
  };
}

export function loadInfoFail(error) {
  return {
    type: LOAD_SERVER_INFO_FAIL,
    error
  };
}
