import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
// import {reducer as reduxAsyncConnect} from 'redux-async-connect';
// import {reducer as notifications} from 'react-notification-system-redux';
// polyfill
import 'babel-polyfill';

import handleServerInfo from './serverInfo';

const rootReducer = combineReducers({
  routing: routerReducer,
  handleServerInfo
  // reduxAsyncConnect,
  // notifications,
});

export default rootReducer;
