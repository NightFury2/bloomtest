import {
  LOAD_SERVER_INFO_FAIL,
  LOAD_SERVER_INFO_SUCCESS,
  LOAD_SERVER_INFO
} from '../actions/actionTypes';

export default function handleServerInfo(
  state = {
    message: '',
    time: 0,
    isLoading: false
  }, action) {
  switch (action.type) {
    case LOAD_SERVER_INFO:
      return Object.assign({}, state, {
        isLoading: true,
        success: false,
        message: '',
        time: ''
      });
    case LOAD_SERVER_INFO_SUCCESS:
      return Object.assign({}, state, {
        success: true,
        isLoading: false,
        message: action.message,
        time: action.time
      });
    case LOAD_SERVER_INFO_FAIL:
      return Object.assign({}, state, {
        isLoading: false,
        data: [],
        error: action.error
      });
    default:
      return state;
  }
}
