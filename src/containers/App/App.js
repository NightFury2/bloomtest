import React from 'react';
// import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import Helmet from 'react-helmet';
import config from '../../config';
// import {getServerInfo} from '../../sagas/serverInfo';

const styles = {
  root: {
    background: 'red',
  },
};

@withStyles(styles)
export default class App extends React.Component {
  static propTypes = {
    children: React.PropTypes.object.isRequired,
    classes: React.PropTypes.object.isRequired
  };

  render() {
    return (
      <div className={this.props.classes.root}>
        <Helmet {...config.app.head}/>

        <div className="navbar">

        </div>

        <div>
          {this.props.children}
        </div>

      </div>
    );
  }
}

// function preload() {
//   return [
//     [getServerInfo]
//   ];
// }
// App.preload = preload;
