// import has from 'lodash/has';
import React from 'react';
import { Provider } from 'react-redux';
import { Router, RouterContext } from 'react-router';
// import GoogleAnalytics from 'react-ga';

export default class Root extends React.Component {
  static propTypes = {
    store: React.PropTypes.object.isRequired,
    history: React.PropTypes.object.isRequired,
    routes: React.PropTypes.node.isRequired,
    type: React.PropTypes.object,
    renderProps: React.PropTypes.object
  };

  onUpdate = () => {
    const { type } = this.props;
    if (type !== 'server') {
      // const state = store.getState();
      // if (has(state, 'router.pathname')) {
      //   GoogleAnalytics.pageview(state.router.pathname);
      // }
    }
  };
  render() {
    const { store, history, routes, type, renderProps } = this.props;
    return (
      <Provider store={store}>
        <div>
          {type === 'server'
            ? <RouterContext {...renderProps} />
            : <Router history={history} routes={routes} onUpdate={this.onUpdate} />}
        </div>
      </Provider>
    );
  }
}
